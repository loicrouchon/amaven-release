package me.experiment.amaven.release;

import org.junit.Test;

import static org.assertj.core.api.Assertions.fail;

public class BranchVersionPatternTest {

    @Test
    public void shouldNotThrowOnSemanticVersionPattern() {
        try {
            new BranchVersionPattern("branch", "${MAJOR}.${MINOR}.${BUILD}");
        } catch (IllegalArgumentException e) {
            fail("fail to build BranchVersionPattern", e);
        }
    }
    @Test
    public void shouldNotThrowOnSemanticVersionPatternWithPrefix() {
        try {
            new BranchVersionPattern("branch", "dev-24-${MAJOR}.${MINOR}.${BUILD}");
        } catch (IllegalArgumentException e) {
            fail("fail to build BranchVersionPattern", e);
        }
    }
    @Test
    public void shouldNotThrowOnSemanticVersionWithSuffix() {
        try {
            new BranchVersionPattern("branch", "${MAJOR}.${MINOR}.${BUILD}-dev-42");
        } catch (IllegalArgumentException e) {
            fail("fail to build BranchVersionPattern", e);
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotThrowIfNoBuildPlaceholderIsSpecified() {
        new BranchVersionPattern("branch", "${MAJOR}.${MINOR}");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotThrowIfNoBuildPlaceholderIsMajorIsMissing() {
        new BranchVersionPattern("branch", "${BUILD}");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotThrowIfNoBuildPlaceholderIsMinorIsMissing() {
        new BranchVersionPattern("branch", "${MAJOR}.${BUILD}");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowOnVersionPatternWithStringInTheMiddle() {
        new BranchVersionPattern("branch", "${MAJOR}.${MINOR}-dev-${BUILD}");
    }
}