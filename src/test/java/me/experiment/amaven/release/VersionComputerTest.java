package me.experiment.amaven.release;


import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class VersionComputerTest {

    private static final Version VERSION_1_1 = new Version(1, 1);

    @Test
    public void shouldNotComputeVersionIfBranchNotFound() {
        Repository repository = new RepositoryStub();
        Project project = new ProjectStub(VERSION_1_1);
        BranchVersionPatternMapper branchVersionPatternMapper = branches();
        VersionComputer versionComputer = new VersionComputer(branchVersionPatternMapper, repository, project);
        String newVersion = versionComputer.computeVersionForBranch("branch");
        assertThat(newVersion).isEqualTo(null);
    }

    @Test
    public void shouldComputeVersionForFirstReleaseBranch() {
        Repository repository = new RepositoryStub();
        Project project = new ProjectStub(new Version(1, 1));
        BranchVersionPatternMapper branchVersionPatternMapper = branches(
                new BranchVersionPattern("release/.*", "${MAJOR}.${MINOR}.${BUILD}")
        );
        VersionComputer versionComputer = new VersionComputer(branchVersionPatternMapper, repository, project);
        String newVersion = versionComputer.computeVersionForBranch("release/1.0");
        assertThat(newVersion).isEqualTo("1.1.0");
    }

    @Test
    public void shouldComputeVersionForNextReleaseBranch() {
        Repository repository = new RepositoryStub("1.1.0", "1.1.5", "1.1.2");
        Project project = new ProjectStub(new Version(1, 1));
        BranchVersionPatternMapper branchVersionPatternMapper = branches(
                new BranchVersionPattern("release/.*", "${MAJOR}.${MINOR}.${BUILD}")
        );
        VersionComputer versionComputer = new VersionComputer(branchVersionPatternMapper, repository, project);
        String newVersion = versionComputer.computeVersionForBranch("release/1.0");
        assertThat(newVersion).isEqualTo("1.1.6");
    }

    @Test
    public void shouldComputeVersionForFirstNonReleaseBranchWithPrefix() {
        Repository repository = new RepositoryStub();
        Project project = new ProjectStub(new Version(1, 1));
        BranchVersionPatternMapper branchVersionPatternMapper = branches(
                new BranchVersionPattern("develop", "dev-${MAJOR}.${MINOR}.${BUILD}")
        );
        VersionComputer versionComputer = new VersionComputer(branchVersionPatternMapper, repository, project);
        String newVersion = versionComputer.computeVersionForBranch("develop");
        assertThat(newVersion).isEqualTo("dev-1.1.0");
    }

    @Test
    public void shouldComputeVersionForFirstNonReleaseBranchWithSuffix() {
        Repository repository = new RepositoryStub();
        Project project = new ProjectStub(new Version(1, 1));
        BranchVersionPatternMapper branchVersionPatternMapper = branches(
                new BranchVersionPattern("develop", "${MAJOR}.${MINOR}.${BUILD}-dev")
        );
        VersionComputer versionComputer = new VersionComputer(branchVersionPatternMapper, repository, project);
        String newVersion = versionComputer.computeVersionForBranch("develop");
        assertThat(newVersion).isEqualTo("1.1.0-dev");
    }

    @Test
    public void shouldComputeVersionForNextNonReleaseBranchWithPrefix() {
        Repository repository = new RepositoryStub("dev-1.1.0", "dev-1.1.5", "dev-1.1.2");
        Project project = new ProjectStub(new Version(1, 1));
        BranchVersionPatternMapper branchVersionPatternMapper = branches(
                new BranchVersionPattern("develop", "dev-${MAJOR}.${MINOR}.${BUILD}")
        );
        VersionComputer versionComputer = new VersionComputer(branchVersionPatternMapper, repository, project);
        String newVersion = versionComputer.computeVersionForBranch("develop");
        assertThat(newVersion).isEqualTo("dev-1.1.6");
    }

    @Test
    public void shouldComputeVersionForNextNonReleaseBranchWithSuffix() {
        Repository repository = new RepositoryStub("1.1.0-dev", "1.1.5-dev", "1.1.2-dev");
        Project project = new ProjectStub(new Version(1, 1));
        BranchVersionPatternMapper branchVersionPatternMapper = branches(
                new BranchVersionPattern("develop", "${MAJOR}.${MINOR}.${BUILD}-dev")
        );
        VersionComputer versionComputer = new VersionComputer(branchVersionPatternMapper, repository, project);
        String newVersion = versionComputer.computeVersionForBranch("develop");
        assertThat(newVersion).isEqualTo("1.1.6-dev");
    }

    private BranchVersionPatternMapper branches(BranchVersionPattern... branchesVersionPatterns) {
        return new BranchVersionPatternMapper(new HashSet<>(Arrays.asList(branchesVersionPatterns)));
    }

    private static class RepositoryStub implements Repository {


        private final Set<String> matchingVersions;

        RepositoryStub(String... matchingVersions) {
            this.matchingVersions = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(matchingVersions)));
        }

        @Override
        public Set<String> listVersionsMatchingPattern(String pattern) {
            return matchingVersions;
        }
    }

    private static class ProjectStub implements Project {
        private final Version version;

        public ProjectStub(Version version) {
            this.version = version;
        }

        @Override
        public Version read() {
            return version;
        }
    }
}
