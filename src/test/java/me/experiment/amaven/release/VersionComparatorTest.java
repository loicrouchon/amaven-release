package me.experiment.amaven.release;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class VersionComparatorTest {

    private static final VersionComparator VERSION_COMPARATOR = new VersionComparator();

    @Test
    public void shouldCompareTwoEqualsVersions() throws Exception {
        assertThat(VERSION_COMPARATOR.compare("1.0.0", "1.0.0")).isEqualTo(0);
    }

    @Test
    public void shouldCompareVersions1to2() throws Exception {
        assertThat(VERSION_COMPARATOR.compare("1.0.0", "2.0.0")).isEqualTo(-1);
    }

    @Test
    public void shouldCompareVersions2to1() throws Exception {
        assertThat(VERSION_COMPARATOR.compare("2.0.0", "1.0.0")).isEqualTo(1);
    }

    @Test
    public void shouldCompareVersions1_2_3to1_2_4() throws Exception {
        assertThat(VERSION_COMPARATOR.compare("1.2.3", "1.2.4")).isEqualTo(-1);
    }

    @Test
    public void shouldCompareVersions1_2_4to1_2_3() throws Exception {
        assertThat(VERSION_COMPARATOR.compare("1.2.4", "1.2.3")).isEqualTo(1);
    }

    @Test
    public void shouldCompareVersions1_12_1to1_2_3() throws Exception {
        assertThat(VERSION_COMPARATOR.compare("1.12.1", "1.2.3")).isEqualTo(1);
    }

    @Test
    public void shouldCompareVersions1_2_3to1_12_1() throws Exception {
        assertThat(VERSION_COMPARATOR.compare("1.2.3", "1.12.1")).isEqualTo(-1);
    }

    @Test
    public void shouldCompareVersions1_12_1to1_12() throws Exception {
        assertThat(VERSION_COMPARATOR.compare("1.12.1", "1.12")).isEqualTo(1);
    }

    @Test
    public void shouldCompareVersions1_12to1_12_1() throws Exception {
        assertThat(VERSION_COMPARATOR.compare("1.12", "1.12.1")).isEqualTo(-1);
    }
}