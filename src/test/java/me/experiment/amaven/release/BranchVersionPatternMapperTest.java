package me.experiment.amaven.release;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class BranchVersionPatternMapperTest {

    @Test
    public void shouldNotReturnVersionPatternIfBranchNotFound() {
        BranchVersionPatternMapper mapper = new BranchVersionPatternMapper(
                Collections.<BranchVersionPattern>emptySet());
        assertThat(mapper.getVersionPatternForBranch("unknown-branch")).isEqualTo(null);
    }

    @Test
    public void shouldReturnVersionPatternIfBranchFound() {
        BranchVersionPattern branchVersionPattern = new BranchVersionPattern("develop",
                "dev-${MAJOR}.${MINOR}.${BUILD}");
        BranchVersionPatternMapper mapper = new BranchVersionPatternMapper(set(branchVersionPattern));
        assertThat(mapper.getVersionPatternForBranch("develop")).isEqualTo(branchVersionPattern);
    }

    static final Set<BranchVersionPattern> set(BranchVersionPattern... patterns) {
        return new HashSet<>(Arrays.asList(patterns));
    }
}