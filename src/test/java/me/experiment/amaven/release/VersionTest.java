package me.experiment.amaven.release;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class VersionTest {

    private static final Version version = new Version(1, 0);

    @Test
    public void shouldProduceReleaseBranchPattern() {
        assertThat(version.pattern("${MAJOR}.${MINOR}.${BUILD}")).isEqualTo("1\\.0\\.[0-9]+");
    }

    @Test
    public void shouldProduceNonReleaseBranchPrefixPattern() {
        assertThat(version.pattern("dev-${MAJOR}.${MINOR}.${BUILD}")).isEqualTo("dev-1\\.0\\.[0-9]+");
    }

    @Test
    public void shouldProduceNonReleaseBranchSuffixPattern() {
        assertThat(version.pattern("${MAJOR}.${MINOR}.${BUILD}-dev")).isEqualTo("1\\.0\\.[0-9]+-dev");
    }

    @Test
    public void shouldComputeDefaultReleaseBranchVersion() {
        assertThat(version.defaultVersion("${MAJOR}.${MINOR}.${BUILD}")).isEqualTo("1.0.0");
    }

    @Test
    public void shouldComputeDefaultNonReleaseBranchWithPrefixVersion() {
        assertThat(version.defaultVersion("dev-${MAJOR}.${MINOR}.${BUILD}")).isEqualTo("dev-1.0.0");
    }

    @Test
    public void shouldComputeDefaultNonReleaseBranchWithSuffixVersion() {
        assertThat(version.defaultVersion("${MAJOR}.${MINOR}.${BUILD}-dev")).isEqualTo("1.0.0-dev");
    }

    @Test
    public void shouldIncrementReleaseBranchVersion() {
        assertThat(version.increment("${MAJOR}.${MINOR}.${BUILD}", "1.0.5")).isEqualTo("1.0.6");
    }

    @Test
    public void shouldIncrementNonReleaseBranchWithPrefixVersion() {
        assertThat(version.increment("dev-${MAJOR}.${MINOR}.${BUILD}", "dev-1.0.5")).isEqualTo("dev-1.0.6");
    }

    @Test
    public void shouldIncrementNonReleaseBranchWithSuffixVersion() {
        assertThat(version.increment("${MAJOR}.${MINOR}.${BUILD}-dev", "1.0.5-dev")).isEqualTo("1.0.6-dev");
    }
}