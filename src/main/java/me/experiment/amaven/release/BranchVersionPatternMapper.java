package me.experiment.amaven.release;

import java.util.Collections;
import java.util.Set;

class BranchVersionPatternMapper {

    private final Set<BranchVersionPattern> branchVersionPatterns;

    public BranchVersionPatternMapper(Set<BranchVersionPattern> branchVersionPatterns) {
        this.branchVersionPatterns = Collections.unmodifiableSet(branchVersionPatterns);
    }

    public BranchVersionPattern getVersionPatternForBranch(String branchName) {
        for (BranchVersionPattern branchVersionPattern : branchVersionPatterns) {
            if (branchVersionPattern.matchBranch(branchName)) {
                return branchVersionPattern;
            }
        }
        return null;
    }
}
