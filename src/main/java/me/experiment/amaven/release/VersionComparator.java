package me.experiment.amaven.release;

import java.util.Comparator;

public class VersionComparator implements Comparator<String> {

    @Override
    public int compare(String v1, String v2) {
        String[] v1Fragments = splitVersionFragments(v1);
        String[] v2Fragments = splitVersionFragments(v2);
        for (int i = 0; i < Math.min(v1Fragments.length, v2Fragments.length); i++) {
            int v1Digit = Integer.parseInt(v1Fragments[i]);
            int v2Digit = Integer.parseInt(v2Fragments[i]);
            if (v1Digit > v2Digit) {
                return 1;
            }
            if (v1Digit < v2Digit) {
                return -1;
            }
        }
        return Integer.compare(v1Fragments.length, v2Fragments.length);
    }

    private String[] splitVersionFragments(String version) {
        if (!version.matches("\\d+(?:\\.\\d+)*")) {
            throw new IllegalArgumentException("Version should follow semantic versioning but is: '" + version + "'");
        }
        return version.split("\\.");
    }
}
