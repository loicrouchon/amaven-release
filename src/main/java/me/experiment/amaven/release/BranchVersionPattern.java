package me.experiment.amaven.release;

import java.util.Objects;
import java.util.Set;

class BranchVersionPattern {

    static final String QUALIFIER_PATTERN = "[a-zA-Z][a-zA-Z0-9-]*";

    private static final String BRANCH_VERSION_VALIDATION_PATTERN =
            "(?:" + QUALIFIER_PATTERN + "-)?\\$\\{MAJOR\\}.\\$\\{MINOR\\}.\\$\\{BUILD\\}(?:-" + QUALIFIER_PATTERN + ")?";

    private final String branchNamePattern;

    private final String branchVersionPattern;

    public BranchVersionPattern(String branchNamePattern, String branchVersionPattern) {
        if (!branchVersionPattern.matches(BRANCH_VERSION_VALIDATION_PATTERN)) {
            throw new IllegalArgumentException("branch version pattern should match '" +
                    BRANCH_VERSION_VALIDATION_PATTERN + "' but is '" + branchVersionPattern + "'");
        }
        this.branchNamePattern = branchNamePattern;
        this.branchVersionPattern = branchVersionPattern;
    }

    public boolean matchBranch(String branchName) {
        return branchName.matches(branchNamePattern);
    }

    public Set<String> getExistingVersionsMatchingPattern(Version version, Repository repository) {
        String versionPattern = version.pattern(branchVersionPattern);
        return repository.listVersionsMatchingPattern(versionPattern);
    }

    public String incrementVersion(Version version, String highestVersion) {
        return version.increment(branchVersionPattern, highestVersion);
    }

    public String defaultVersion(Version version) {
        return version.defaultVersion(branchVersionPattern);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BranchVersionPattern that = (BranchVersionPattern) o;
        return Objects.equals(branchNamePattern, that.branchNamePattern) &&
                Objects.equals(branchVersionPattern, that.branchVersionPattern);
    }

    @Override
    public int hashCode() {
        return Objects.hash(branchNamePattern, branchVersionPattern);
    }

    @Override
    public String toString() {
        return "BranchVersionPattern{" +
                "branchNamePattern='" + branchNamePattern + '\'' +
                ", branchVersionPattern='" + branchVersionPattern + '\'' +
                '}';
    }
}
