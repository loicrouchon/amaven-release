package me.experiment.amaven.release;

import java.util.Set;

public class VersionComputer {

    private final BranchVersionPatternMapper branchVersionPatternMapper;

    private final Repository repository;

    private final Project project;

    public VersionComputer(BranchVersionPatternMapper branchVersionPatternMapper,
                           Repository repository,
                           Project project) {
        this.branchVersionPatternMapper = branchVersionPatternMapper;
        this.repository = repository;
        this.project = project;
    }


    public String computeVersionForBranch(String branch) {
        BranchVersionPattern branchVersionPattern = branchVersionPatternMapper.getVersionPatternForBranch(branch);
        if (branchVersionPattern == null) {
            return null;
        }
        Version currentProjectVersion = project.read();
        Set<String> existingVersions = branchVersionPattern.getExistingVersionsMatchingPattern(currentProjectVersion, repository);
        return computeNewVersion(branchVersionPattern, currentProjectVersion, existingVersions);
    }

    private String computeNewVersion(BranchVersionPattern branchVersionPattern,
                                     Version currentProjectVersion,
                                     Set<String> existingVersions) {
        String newVersion;
        if (!existingVersions.isEmpty()) {
            String highestVersion = findHighestVersion(existingVersions);
            newVersion = incrementVersion(branchVersionPattern, currentProjectVersion, highestVersion);
        } else {
            newVersion = branchVersionPattern.defaultVersion(currentProjectVersion);
        }
        return newVersion;
    }

    private String incrementVersion(BranchVersionPattern branchVersionPattern, Version version, String highestVersion) {
        String[] versionFragments = removePrefixAndSuffix(highestVersion).split("\\.");
        String build = versionFragments[2];
        return branchVersionPattern.incrementVersion(version, highestVersion);
    }

    private String findHighestVersion(Set<String> versions) {
        String highestVersion = versions.iterator().next();
        VersionComparator versionComparator = new VersionComparator();
        for (String version : versions) {
            if (versionComparator.compare(removePrefixAndSuffix(version), removePrefixAndSuffix(highestVersion)) > 0) {
                highestVersion = version;
            }
        }
        return highestVersion;
    }

    private String removePrefixAndSuffix(String version) {
        return version
                .replaceFirst("^" + BranchVersionPattern.QUALIFIER_PATTERN + "-", "")
                .replaceFirst("-" + BranchVersionPattern.QUALIFIER_PATTERN + "$", "");
    }
}
