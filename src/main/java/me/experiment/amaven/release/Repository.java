package me.experiment.amaven.release;

import java.util.Set;

interface Repository {
    Set<String> listVersionsMatchingPattern(String pattern);
}
