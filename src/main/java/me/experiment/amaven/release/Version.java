package me.experiment.amaven.release;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Version {

    private final String major;

    private final String minor;

    public Version(int major, int minor) {
        this.major = Integer.toString(major);
        this.minor = Integer.toString(minor);
    }

    public String pattern(String branchVersionPattern) {
        return majorAndMinor(branchVersionPattern.replace(".", "\\.")).replace("${BUILD}", "[0-9]+");
    }

    private String majorAndMinor(String branchVersionPattern) {
        return branchVersionPattern
                .replace("${MAJOR}", major)
                .replace("${MINOR}", minor);
    }

    public String increment(String branchVersionPattern, String highestVersion) {
        String pattern = majorAndMinor(branchVersionPattern.replace(".", "\\.")).replace("${BUILD}", "([0-9]+)");
        Matcher matcher = Pattern.compile(pattern).matcher(highestVersion);
        if (matcher.matches()) {
            String build = matcher.group(1);
            return newVersion(branchVersionPattern, Integer.parseInt(build) + 1);

        }
        return defaultVersion(branchVersionPattern);
    }

    private String newVersion(String branchVersionPattern, int buildNumber) {
        return majorAndMinor(branchVersionPattern).replace("${BUILD}", Integer.toString(buildNumber));
    }

    public String defaultVersion(String branchVersionPattern) {
        return newVersion(branchVersionPattern, 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Version version = (Version) o;
        return Objects.equals(major, version.major) &&
                Objects.equals(minor, version.minor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(major, minor);
    }

    @Override
    public String toString() {
        return "Version{" +
                "major='" + major + '\'' +
                ", minor='" + minor + '\'' +
                '}';
    }
}
